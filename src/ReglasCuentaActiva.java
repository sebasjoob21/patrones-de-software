public class ReglasCuentaActiva implements Regla {
    @Override
    public String execute(Transaccion transaccion) {
        if (!transaccion.cuenta.activa){
            return "La cuenta no esta activa ";

        }
        return "";

    }
}
