import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Validador {

    private final List<Regla> rules;

    public Validador(List<Regla> rules) {
        this.rules = rules;
        System.out.println("constructor del validador ");

    }

    List<String> validarTransaccion(Transaccion transaction) {
        List<String> reglasVioladas = new ArrayList<>();


        for (Regla rule : this.rules) {
            String response = rule.execute(transaction);
            if (!response.isEmpty()) {
                reglasVioladas.add(response);
            }
        }

        return reglasVioladas;
    }
}

