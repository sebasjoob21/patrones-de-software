public class ReglaMontoValido  implements Regla{
    @Override
    public String execute(Transaccion transaccion){
        if(transaccion.montoMovimiento <= 0 ){
            return "el monto es valido ";
        }
        return "";
    }
}
