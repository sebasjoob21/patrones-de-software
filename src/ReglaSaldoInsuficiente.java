public class ReglaSaldoInsuficiente implements Regla {
    @Override
    public String execute(Transaccion transaccion){
        if(transaccion.montoMovimiento > transaccion.cuenta.monto){
            return "Saldo insuficiente ";
        }
        return "";
    }
}
