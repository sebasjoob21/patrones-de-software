import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {


        System.out.println("Compilado");
        Cuenta cuentaSebas = new Cuenta();
        cuentaSebas.nombreUsuario = "servio";
        cuentaSebas.activa = false;
        cuentaSebas.tipo = "AH";
        cuentaSebas.monto = 100.0;

        Transaccion pagoSemestre = new Transaccion();
        pagoSemestre.fecha = new Date();
        pagoSemestre.montoMovimiento = -1.0;
        pagoSemestre.historico = new ArrayList<>();
        pagoSemestre.cuenta = cuentaSebas;

        List<Regla>rules =new ArrayList<>();
        rules.add(new ReglasCuentaActiva());
        rules.add(new ReglaMontoValido());
        rules.add(new ReglaSaldoInsuficiente());

        Validador validador = new Validador(rules);
        List<String> reglasVioladas = validador.validarTransaccion(pagoSemestre);

        System.out.println(cuentaSebas.nombreUsuario + "monto" + cuentaSebas.monto);
        System.out.println(pagoSemestre.fecha + "montoTransaccion" + pagoSemestre.montoMovimiento);
        System.out.println(reglasVioladas.stream().collect(Collectors.joining(",")));


    }
}